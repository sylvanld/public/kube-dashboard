## SQUIDploy architecture
* Most standard approach consist in letting devops configure how to release and install helm chart on K8S from their pipelines.

It gives them lot of flexibility, but a small change in infrastructure may impact them a lot.

![Simple CI/CD](docs/diagrams/helm-ci-cd-simple.drawio.png)

* Instead we could just give devops complete freedom of choices on which helm repo they want to use, and let them packages their helm chart in their own fashion. But, standardize deployments using a component responsible for deployment that abstract infrastructure complexity.

Developers simply publish their helm chart, and give `deployator` a read only token required to pull the chart. Then deployments are standardized through a REST API.

![More robust CI/CD](docs/diagrams/helm-ci-cd-deployator.drawio.png)

Deployator is a service running on top of K8S. It is deployed by region. Here is an architecture diagrams of its components.

![deployator architecture](docs/diagrams/deployator-architecture.drawio.png)

## Run with docker-compose

* create network `squidploy`

* run all services using

```
make compose-up
```

## Brainstorming / Roadmap

* Registry authentication per container!

https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
