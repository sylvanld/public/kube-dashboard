
COMPOSE=docker-compose								\
		-f services/api/docker-compose.yml			\
		-f services/ui/docker-compose.yml

help:
	...

compose-up:
	$(COMPOSE) up

compose-down:
	$(COMPOSE) down

compose-build:
	$(COMPOSE) build
