import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '@/views/HomeView'
import DashboardView from '@/views/dashboard/DashboardView'
import AppConfigView from '@/views/dashboard/AppConfigView'
// dashboard plugins views... todo lazy loading...
import EditSecretView from '@/views/dashboard/EditSecretView'
import DeploymentOverview from '@/views/dashboard/DeploymentOverview'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/dashboard/:namespace/:appId',
    name: 'dashboard',
    component: DashboardView,
    children: [
      {
        path: 'overview',
        name: 'app.overview',
        component: DeploymentOverview
      },
      {
        path: 'config',
        name: 'app.config',
        component: AppConfigView
      },
      {
        path: 'secrets',
        name: 'app.secrets.edit',
        component: EditSecretView
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
