export default {
    namespaced: true,
    state: {
        pluginsConfig: {
            "logging": {
                enabled: false
            },
            "metrics": {
                enabled: false
            },
            "events": {
                enabled: false
            },
            "access-logs": {
                enabled: false
            },
            "access-control": {
                enabled: false
            },
            "permissions": {
                enabled: false
            },
            "configuration": {
                enabled: false
            }
        },
    },
    getters: {
        enabledPlugins(state){
            return Object.keys(state.pluginsConfig).filter(key=>state.pluginsConfig[key].enabled === true)
        },
        installablePlugins(state) {
            return Object.keys(state.pluginsConfig).filter(key=>state.pluginsConfig[key].enabled === false)
        }
    },
    mutations: {
        addPlugin(state, plugin){
            state.pluginsConfig[plugin].enabled = true;
        }
    }
}
