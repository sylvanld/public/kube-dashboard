"""Defines constants settings from environment variables"""
import os


def _requires_env(key: str):
    value = os.getenv(key)
    if value is None:
        raise ValueError(f"Missing environment variable: {key}")
    return value


DATABASE_URL = _requires_env("DATABASE_URL")

PAGINATION_DEFAULT_SIZE = os.getenv("PAGINATION_DEFAULT_SIZE", 50)
PAGINATION_MAXIMUM_SIZE = os.getenv("PAGINATION_MAXIMUM_SIZE", 100)
