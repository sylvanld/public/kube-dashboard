from typing import Generic, List, TypeVar

from pydantic.generics import GenericModel

T = TypeVar("T", bound=GenericModel)


class PaginationDTO(GenericModel, Generic[T]):
    items: List[T]
    count: int
    page: int
    size: int
