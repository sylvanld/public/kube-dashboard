from deployator.api import apps, groups, integrations
from deployator.api.tags import API_TAGS
from deployator.db.middleware import DatabaseMiddleware
from deployator.settings import DATABASE_URL
from fastapi import FastAPI


def create_api():
    api = FastAPI(
        title="Deployator API",
        description="This API is designed to be your main entrypoint to deploy applications on kubernetes. It handles only deployments using helm charts, hosted externally!",
        docs_url="/",
        openapi_tags=API_TAGS,
    )

    api.include_router(groups.router)
    api.include_router(apps.router)
    api.include_router(integrations.router)

    # todo: do something with this...
    DatabaseMiddleware(DATABASE_URL)

    return api
