API_TAGS = [
    {
        "name": "apps",
        "description": "An application correspond to a deployment in Kubernetes. It is the base unit to interface with integrations like configuration provider, logs aggregator, etc...",
    },
    {
        "name": "groups",
        "description": "A group is a logical unit used to group a bunch of applications, usually managed by the same team.",
    },
    {
        "name": "integrations",
        "description": "Addons that can be added to application to simplify monitoring, configuration management, etc...",
    },
]
