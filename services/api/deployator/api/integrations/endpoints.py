from fastapi import APIRouter

INTEGRATIONS = ("logs-data-platform", "events", "configuration")

router = APIRouter(tags=["integrations"])


@router.get("/integrations")
def describe_all_available_integrations():
    ...


@router.get("/apps/{app_id}/integrations")
def list_integrations_enabled_for_app():
    ...


@router.post("/apps/{app_id}/integrations/{integration}/enable")
def enable_integration():
    ...


@router.post("/apps/{app_id}/integrations/{integration}/disable")
def disable_integration():
    ...
