from deployator.api.apps.dtos import (
    ApplicationReadDTO,
    ApplicationWriteDTO,
    PaginatedAppsDTO,
)
from deployator.db.middleware import first_result, paginate_result, session
from deployator.db.models import DBApplication, DBGroup
from deployator.settings import PAGINATION_DEFAULT_SIZE
from fastapi import APIRouter

router = APIRouter(tags=["apps"])


@router.get("/groups/{group_id}/apps", response_model=PaginatedAppsDTO)
def list_apps_in_group(
    group_id: str, page: int = 0, size: int = PAGINATION_DEFAULT_SIZE
):
    query = session.query(DBApplication).filter_by(group_id=group_id)
    return paginate_result(query, page=page, size=size)


@router.post("/groups/{group_id}/apps", response_model=ApplicationReadDTO)
def declare_new_app(app_dto: ApplicationWriteDTO, group_id: str):
    group: DBGroup = first_result(session.query(DBGroup).filter_by(id=group_id))
    new_app = DBApplication(**app_dto.dict(), group_id=group.id)
    session.add(new_app)
    session.commit()
    return new_app


@router.get("/apps/{app_id}", response_model=ApplicationReadDTO)
def get_app_details(app_id: str):
    return first_result(session.query(DBApplication).filter_by(id=app_id))
