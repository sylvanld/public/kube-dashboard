from deployator.api.common.dtos import PaginationDTO
from pydantic import BaseModel


class ApplicationReadDTO(BaseModel):
    class Config:
        orm_mode = True

    alias: str
    name: str
    description: str


class ApplicationWriteDTO(BaseModel):
    alias: str
    name: str
    description: str


class PaginatedAppsDTO(PaginationDTO[ApplicationReadDTO]):
    ...
