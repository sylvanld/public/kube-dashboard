from typing import List

from deployator.api.groups.dtos import GroupReadDTO, GroupWriteDTO, PaginatedGroupsDTO
from deployator.db.middleware import first_result, paginate_result, session
from deployator.db.models import DBGroup
from deployator.settings import PAGINATION_DEFAULT_SIZE
from fastapi import APIRouter

router = APIRouter(prefix="/groups", tags=["groups"])


@router.post("", response_model=GroupReadDTO)
def create_group(group_dto: GroupWriteDTO):
    new_group = DBGroup(**group_dto.dict())
    session.add(new_group)
    session.commit()
    print(new_group)
    return new_group


@router.get("", response_model=PaginatedGroupsDTO)
def list_groups(page: int = 0, size: int = PAGINATION_DEFAULT_SIZE):
    return paginate_result(session.query(DBGroup), page=page, size=size)


@router.get("/{group_id}", response_model=GroupReadDTO)
def get_group_details(group_id: int):
    return first_result(session.query(DBGroup).filter_by(id=group_id))
