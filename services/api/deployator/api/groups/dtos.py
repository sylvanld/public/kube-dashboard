from deployator.api.common.dtos import PaginationDTO
from pydantic import BaseModel


class GroupWriteDTO(BaseModel):
    class Config:
        orm_mode = True

    alias: str
    name: str
    description: str


class GroupReadDTO(BaseModel):
    class Config:
        orm_mode = True

    alias: str
    name: str
    description: str


class PaginatedGroupsDTO(PaginationDTO[GroupReadDTO]):
    ...
