"""This modules instanciate API class."""
from deployator.api.factory import create_api

api = create_api()
