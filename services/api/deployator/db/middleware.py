from typing import TypeVar

from deployator.db.exception import DBObjectNotFound
from deployator.settings import PAGINATION_MAXIMUM_SIZE
from pydantic import BaseModel
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Query, scoped_session, sessionmaker

T = TypeVar("T")


def first_result(query: Query):
    item = query.first()
    if item is None:
        raise DBObjectNotFound()
    return item


def paginate_result(query: Query, page: int, size: int):
    if size > PAGINATION_MAXIMUM_SIZE:
        size = PAGINATION_MAXIMUM_SIZE

    items = query.offset(page * size).limit(size).all()

    return {"items": items, "count": len(items), "page": page, "size": size}


DBModel = declarative_base()

_session_factory = sessionmaker()
session = scoped_session(session_factory=_session_factory)


class DatabaseMiddleware:
    def __init__(self, database_url: str):
        self.engine = create_engine(database_url)
        DBModel.metadata.create_all(bind=self.engine)
        _session_factory.configure(bind=self.engine)
