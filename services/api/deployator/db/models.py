from deployator.db.middleware import DBModel
from sqlalchemy import Column, ForeignKey, Integer, String


class DBGroup(DBModel):
    __tablename__ = "groups"

    id = Column(Integer, primary_key=True)
    alias = Column(String(45), unique=True, nullable=False)
    name = Column(String(50), nullable=False)
    description = Column(String(400), nullable=True)


class DBApplication(DBModel):
    __tablename__ = "applications"

    id = Column(Integer, primary_key=True)
    alias = Column(String(45), unique=True, nullable=False)
    name = Column(String(50), nullable=False)
    description = Column(String(400), nullable=True)

    group_id = Column(ForeignKey("groups.id"), nullable=False)
